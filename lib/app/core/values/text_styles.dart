import 'package:flutter/material.dart';

import '/app/core/values/app_colors.dart';

const headerTextStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.bold,
  color: AppColors.appBarTextColor,
);


const normalTextStyle = TextStyle(
  fontSize: 13,
  fontWeight: FontWeight.normal,
  color: AppColors.appBarTextColor,
);
