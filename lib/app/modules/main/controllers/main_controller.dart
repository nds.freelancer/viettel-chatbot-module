
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/values/app_values.dart';
import '/app/core/base/base_controller.dart';

class MainController extends BaseController {
  final isExpand = false.obs;
  final currOffset = Offset(Get.width - AppValues.iconSize - AppValues.largePadding, Get.height - AppValues.iconSize - AppValues.largePadding).obs;
  final tempOffset = Offset(Get.width - AppValues.iconSize - AppValues.largePadding, Get.height - AppValues.iconSize - AppValues.largePadding).obs;
  final animationDurationMs = 0.obs;

  onDragUpdate(DragUpdateDetails detail) {
    double dx = detail.delta.dx;
    double dy = detail.delta.dy;
    currOffset.value = Offset(currOffset.value.dx + dx, currOffset.value.dy + dy);
    animationDurationMs.value = 0;
  }

  onDragEnd(DraggableDetails detail) {
    double dx = detail.offset.dx;
    double dy = detail.offset.dy;

    dx = dx.clamp(AppValues.largePadding, Get.width - AppValues.largePadding - AppValues.iconSize);
    dy = dy.clamp(AppValues.largePadding, Get.height - AppValues.largePadding - AppValues.iconSize);

    if (dy != AppValues.largePadding && dy != Get.height - AppValues.largePadding - AppValues.iconSize) {
        if (dx > Get.width / 2) {
          dx = Get.width - AppValues.iconSize - AppValues.largePadding;
        } else {
          dx = AppValues.largePadding;
        }
    }
    final offset = Offset(dx, dy);
    animationDurationMs.value = 200;
    currOffset.value = offset;
  }

  onShowChat() async {
    final offset = Offset(Get.width - AppValues.iconSize - AppValues.largePadding, AppValues.iconSize + AppValues.largePadding);
    animationDurationMs.value = 200;
    tempOffset.value = currOffset.value;
    currOffset.value = offset;
    await Future.delayed(Duration(milliseconds: animationDurationMs.value));
    isExpand.value = true;
  }

  onHideChat() async {
    isExpand.value = false;
    animationDurationMs.value = 200;
    await Future.delayed(Duration(milliseconds: animationDurationMs.value));
    currOffset.value = tempOffset.value;
  }
}
