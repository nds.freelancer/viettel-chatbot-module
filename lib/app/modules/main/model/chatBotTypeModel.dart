import 'dart:convert';

class ChatBotTypeModel {
  String? type;
  String? name;
  String? image;

  ChatBotTypeModel({
    this.type,
    this.name,
    this.image,
  });

  factory ChatBotTypeModel.fromRawJson(String str) => ChatBotTypeModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ChatBotTypeModel.fromJson(Map<String, dynamic> json) => ChatBotTypeModel(
    type: json["type"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "name": name,
    "image": image,
  };
}
