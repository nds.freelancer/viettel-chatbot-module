import 'dart:convert';

class MessageHistoryModel {
  bool? isUser;
  String? jsonData;
  int? timestamp;
  String? type;

  MessageHistoryModel({
    this.isUser,
    this.jsonData,
    this.timestamp,
    this.type,
  });

  factory MessageHistoryModel.fromRawJson(String str) => MessageHistoryModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MessageHistoryModel.fromJson(Map<String, dynamic> json) => MessageHistoryModel(
    isUser: json["isUser"],
    jsonData: json["jsonData"],
    timestamp: json["timestamp"],
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "isUser": isUser,
    "jsonData": jsonData,
    "timestamp": timestamp,
    "type": type,
  };
}
