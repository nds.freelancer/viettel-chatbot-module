
import 'package:flutter/material.dart';
import '../widgets/collapse/collapse_view.dart';
import '../widgets/expand/expand_view.dart';
import '/app/core/base/base_view.dart';
import '/app/modules/main/controllers/main_controller.dart';

class MainView extends BaseView<MainController> {
  @override
  Widget body(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: [
        if (controller.isExpand.value)
          Expand()
        else
          AnimatedPositioned(
            top: controller.currOffset.value.dy,
            left: controller.currOffset.value.dx,
            duration: Duration(milliseconds: controller.animationDurationMs.value),
            child: Collapse(
              onDragUpdate: controller.onDragUpdate,
              onDragEnd: controller.onDragEnd,
              onTap: controller.onShowChat,
            ),
          )
      ],
    );
  }

}
