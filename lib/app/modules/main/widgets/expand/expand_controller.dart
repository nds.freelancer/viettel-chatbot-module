import 'package:chatbot_sdk/app/modules/main/controllers/main_controller.dart';
import 'package:chatbot_sdk/app/modules/main/model/message_history_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';

class ExpandController extends BaseController {
  final count = 0.obs;
  final mainController = Get.find<MainController>();
  final RxList<MessageHistoryModel> messageHistory = RxList.from([
    MessageHistoryModel(type: "button", isUser: false, jsonData: "Xin chào 123123123", timestamp: DateTime.now().millisecondsSinceEpoch),
    MessageHistoryModel(type: "text", isUser: true, jsonData: "JSON DATA", timestamp: DateTime.now().millisecondsSinceEpoch),
  ]);
  final selectedChatBotType = "both".obs;

  final textEditingController = TextEditingController();

  final listViewController = ScrollController();
  void onTap() {
    mainController.onHideChat();
  }

  onSendMessage() async {
    FocusManager.instance.primaryFocus?.unfocus();
    messageHistory.add(MessageHistoryModel(type: "text", isUser: true, jsonData: textEditingController.text, timestamp: DateTime.now().millisecondsSinceEpoch),);
    textEditingController.text = "";
    await Future.delayed(const Duration(seconds: 1));
    messageHistory.add(MessageHistoryModel(type: "text", isUser: false, jsonData: "Đây là câu trả lời tự động", timestamp: DateTime.now().millisecondsSinceEpoch),);
    await Future.delayed(const Duration(milliseconds: 200));
    scrollToBottom();
  }

  scrollToTop() {
    listViewController.animateTo(
        0,
        duration: const Duration(milliseconds: 200),
        curve: Curves.fastOutSlowIn);
  }
  scrollToBottom() {
    if (listViewController.hasClients) {
      listViewController.animateTo(
          listViewController.position.maxScrollExtent ?? 0,
          duration: const Duration(milliseconds: 200),
          curve: Curves.fastOutSlowIn);
    }
  }
}