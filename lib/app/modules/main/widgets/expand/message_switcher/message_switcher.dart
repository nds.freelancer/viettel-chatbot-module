import 'package:chatbot_sdk/app/core/values/text_styles.dart';
import 'package:chatbot_sdk/app/core/widget/chatbot_icon.dart';
import 'package:chatbot_sdk/app/modules/main/model/message_history_model.dart';
import 'package:chatbot_sdk/app/modules/main/widgets/expand/message_switcher/message_type_button.dart';
import 'package:chatbot_sdk/app/modules/main/widgets/expand/message_switcher/message_type_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../../core/values/app_values.dart';

class MessageSwitcherWidget extends StatelessWidget {
  MessageHistoryModel message;

  MessageSwitcherWidget({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: AppValues.smallPadding),
        child: message.isUser! ? _userMessage() : _botMessage());
  }

  _userMessage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        const SizedBox(width: AppValues.avatarSize,),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                padding: const EdgeInsets.all(AppValues.smallPadding),
                decoration: const BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                      bottomLeft: Radius.circular(8),
                    )),
                child: Text(
                  message.jsonData!,
                  softWrap: true,
                  style: normalTextStyle.copyWith(color: Colors.white),
                ),
              ),
              Text(_formatTime(message.timestamp!), style: const TextStyle(color: Colors.grey, fontSize: 12, fontStyle: FontStyle.italic), )
            ],
          ),
        ),
        const SizedBox(
          width: AppValues.smallPadding,
        ),
        const Icon(
          Icons.account_circle_rounded,
          color: Colors.lightBlue,
          size: 30,
        )
      ],
    );
  }

  _botMessage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(
          width: AppValues.avatarSize,
          height: AppValues.avatarSize,
          child: ChatBotIcon()
        ),
        const SizedBox(
          width: AppValues.smallPadding,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _switcher(message),
              Text(_formatTime(message.timestamp!), style: const TextStyle(color: Colors.grey, fontSize: 12, fontStyle: FontStyle.italic), )
            ],
          ),
        ),
        const SizedBox(width: AppValues.avatarSize,),
      ],
    );
  }


  Widget _switcher(MessageHistoryModel message) {
    switch(message.type) {
      case "button":
        return MessageTypeButton(message: message);
      default:
        return MessageTypeText(message: message);
    }
  }

  String _formatTime(millis) {
    var dt = DateTime.fromMillisecondsSinceEpoch(millis);
    var d24 = DateFormat('HH:mm').format(dt);
    return d24;
  }

}
