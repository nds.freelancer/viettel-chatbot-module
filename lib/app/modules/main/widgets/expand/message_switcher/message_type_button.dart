import 'package:chatbot_sdk/app/modules/main/model/message_history_model.dart';
import 'package:flutter/material.dart';

import '../../../../../core/values/app_values.dart';
import '../../../../../core/values/text_styles.dart';

class MessageTypeButton extends StatelessWidget {
  MessageHistoryModel message;
  MessageTypeButton({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.all(AppValues.smallPadding),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
                bottomRight: Radius.circular(8),
              )),
          child: const Text(
            "message.jsonData!",
            softWrap: true,
            style: normalTextStyle,
          ),
        ),
        OutlinedButton(onPressed: () {}, child: const Text("button"))
      ],
    );
  }
}
