import 'package:chatbot_sdk/app/modules/main/model/message_history_model.dart';
import 'package:flutter/material.dart';

import '../../../../../core/values/app_values.dart';
import '../../../../../core/values/text_styles.dart';

class MessageTypeText extends StatelessWidget {
  MessageHistoryModel message;
  MessageTypeText({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppValues.smallPadding),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
            bottomRight: Radius.circular(8),
          )),
      child: Text(
        message.jsonData!,
        softWrap: true,
        style: normalTextStyle,
      ),
    );
  }
}
