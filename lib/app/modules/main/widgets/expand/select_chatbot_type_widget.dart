
import 'package:flutter/material.dart';

import '../../../../core/values/app_values.dart';
import '../../../../core/values/text_styles.dart';
import '../../model/chatBotTypeModel.dart';

class SelectChatBotTypeWidget extends StatelessWidget {

  String selectedChatBotType;
  Function onChange;
   SelectChatBotTypeWidget({Key? key, required this.selectedChatBotType, required this.onChange}) : super(key: key);

  final List<ChatBotTypeModel> chatBotTypes = [
    ChatBotTypeModel(
      type: "myBot",
      name: "MyBot",
      image: "images/icon_chatbot.png"
    ),
    ChatBotTypeModel(
        type: "both",
        name: "MyBot x\nChatGPT",
        image: "images/icon_chatbot.png"
    ),
    ChatBotTypeModel(
        type: "chatGPT",
        name: "ChatGPT",
        image: "images/icon_chatbot.png"
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          "Quý khách muốn trả lời bởi?",
          style: normalTextStyle,
        ),
        const SizedBox(
          height: AppValues.smallPadding,
        ),
        Row(
          children: chatBotTypes.map((e) => _buildChatBotTypeItem(e)).toList()
        )
      ],
    );
  }

  Widget _buildChatBotTypeItem(ChatBotTypeModel item) {
    return Expanded(
        child: GestureDetector(
          onTap: () {
            onChange(item.type);
          },
          child: Container(
                decoration: BoxDecoration(
            border: Border.all(color: selectedChatBotType == item.type ? Colors.red : Colors.black12),
            color: selectedChatBotType == item.type ? Colors.redAccent.withOpacity(0.2) : Colors.white,
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(AppValues.radius_6),
              topRight: Radius.circular(AppValues.radius_6),
              bottomRight: Radius.circular(AppValues.radius_6),
            )),
                height: 50,
                margin: const EdgeInsets.only(right: AppValues.extraSmallPadding),
                padding: const EdgeInsets.symmetric(horizontal: AppValues.smallPadding, vertical: AppValues.extraSmallPadding),
                child: Row(
          children: [
            SizedBox(
              width: AppValues.avatarSize,
              height: AppValues.avatarSize,
              child: Image.asset(item.image!),
            ),
            const SizedBox(width: AppValues.smallPadding,),
            Text(item.name!, style: normalTextStyle, overflow: TextOverflow.ellipsis,)
          ],
                ),
              ),
        ));
  }
}
