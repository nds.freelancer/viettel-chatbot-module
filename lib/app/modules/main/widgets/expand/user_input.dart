import 'package:chatbot_sdk/app/core/values/text_styles.dart';
import 'package:flutter/material.dart';

import '../../../../core/values/app_values.dart';

class UserInput extends StatelessWidget {
  TextEditingController textEditingController;
  Function onButtonMenuTap;
  Function onButtonMicTap;
  Function onButtonSendTap;

  UserInput(
      {Key? key,
      required this.textEditingController,
      required this.onButtonMenuTap,
      required this.onButtonMicTap,
      required this.onButtonSendTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              onButtonMenuTap();
            },
            child: const Icon(Icons.menu),
          ),
          const SizedBox(
            width: AppValues.smallPadding,
          ),
          GestureDetector(
            onTap: () {
              onButtonMicTap();
            },
            child: const Icon(Icons.mic),
          ),
          const SizedBox(
            width: AppValues.smallPadding,
          ),
          Expanded(
              child: TextField(
                style: normalTextStyle,
                controller: textEditingController,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(left: AppValues.padding),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(AppValues.radius_12)),
                hintText: "Nhập nội dung cần hỗ trợ",
                hintStyle: normalTextStyle.copyWith(color: Colors.grey)),
          )),
          const SizedBox(
            width: AppValues.smallPadding,
          ),
          GestureDetector(
            onTap: () {
              onButtonSendTap();
            },
            child: const Icon(Icons.send),
          ),
        ],
      ),
    );
  }
}
